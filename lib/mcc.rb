require "logger"
require "forwardable"
require "ostruct"
require "inifile"
require "json"
require "terminal-table"
require "awesome_print"

require "mcc/version"
require "mcc/app"
require "mcc/local_command"
require "mcc/remote_command"
require "mcc/target"

module Mcc
  class << self
    attr_accessor :app
    attr_accessor :user
  end
end
