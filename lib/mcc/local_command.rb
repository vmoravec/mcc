module Mcc
  class LocalCommand
    LOCALHOST = "localhost"
    Result = Struct.new(:command, :success?, :output, :exit_code, :host)

    extend Forwardable
    def_delegators :@app, :log, :host

    def initialize app
      @app = app
    end

    def exec! command_name, *args
      command = "#{command_name} #{args.join(" ")}".strip
      target = Target.new(host: LOCALHOST)
      result = Result.new(command, false, "", 1000, target)
      log.info("On #{target.host}: `#{command}`")
      IO.popen(command, :err=>[:child, :out]) do |lines|
        lines.each do |line|
          result.output << line
          next unless log.debug?

          log_command_output(line)
        end
      end

      result[:success?] = $?.success?
      result.exit_code = $?.exitstatus
      if !result.success?
        log.error(
          "Command:\n #{command} " +
          "failed with '#{result.output.strip}' " +
          "and status #{result.exit_code} "
        )
        raise LocalCommandFailed.new(result.output)
      end

      return result

    rescue Errno::ENOENT => e
      result.output << "Command `#{command_name}` not found"
      log.error("#{result.host}: #{result.output}")
      raise LocalCommandFailed.new(result)
    end

    def log_command_output line
      case line.chomp
      when /warn|cannot/i
        log.warn(line)
      when /error/i
        log.error(line)
      else
        log.debug(line)
      end
    end

    class LocalCommandFailed < StandardError; end
  end
end
