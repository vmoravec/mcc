module Mcc
  class App
    attr_reader :verbose
    attr_reader :log
    attr_reader :global_options
    attr_reader :host

    def initialize(verbose: false)
      @verbose = verbose
      @local_command = LocalCommand.new(self)
      @host = `hostname 2>&1`.strip
      set_logger
    end

    def command(command_type, target_params={})
      case command_type
        when :local
          @local_command.exec!(target_params[:shell])
        when :remote
          @remote_command ||= RemoteCommand.new(
            self,
            target_params
          ).exec!(target_params[:shell], target_params[:args], env: target_params[:env] || {})
        when nil || command_type.to_s.empty?
          raise 'No command type specified'
        else
          raise "Incorrect command type specified: `command_type`"
      end
    end

    def global_options=(options)
      @global_options = options
      @verbose = options[:verbose]
      set_logger
    end

    private

    def set_logger
      @log = Logger.new(STDOUT)
      if @verbose
        log.level = Logger::DEBUG
        log.formatter = LONG_FORMATTER
      else
        log.level = Logger::INFO
        log.formatter = SIMPLE_FORMATTER
      end
    end

    LONG_FORMATTER = Proc.new do |severity, datetime, progname, message|
      time_format = "%Y-%m-%d %H:%M:%S"
      severity_format = severity == "INFO" ? severity + " " : severity
      "#{severity_format} #{datetime.strftime(time_format)} : #{message.to_s.strip}\n"
    end

    SIMPLE_FORMATTER = Proc.new do |severity, datetime, progname, message|
      "#{message.to_s.strip}\n"
    end
  end
end
