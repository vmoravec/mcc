require "net/ssh"
require "net/ssh/gateway"
require "timeout"

module Mcc
  class RemoteCommand
     EXTENDED_OPTIONS = {
       non_interactive: true,
       keys_only: true,
       number_of_password_prompts: 0,
       port: 22,
       timeout: 5
     }

     Result  = Struct.new(:command, :env, :success?, :output, :error, :exit_code, :host)

     extend Forwardable
     def_delegators :@app, :log, :verbose

     attr_reader :target
     attr_reader :gateway
     attr_reader :session

     def initialize app, target_params
       @app = app
       @target  = Target.new(name: target_params[:host], ip: target_params[:ip])
       log.debug("Target details: #{target}")
       @gateway = OpenStruct.new(target.gateway.attributes) if target.gateway
     end

     def exec! command, args="", env: {}, capture_error: true
       raise "Command cannot be empty" if command.empty?

       log.level = ::Logger::WARN unless verbose
       connect!

       environment = set_environment(env)
       command = command.is_a?(String) ? command : command.join(" ")
       full_command = "#{command} #{args}".strip
       result = Result.new(full_command, environment, false, "", "", 1000, target)

       open_session_channel do |channel|
         channel.exec("#{environment}#{full_command}") do |p, d|
           log.warn(
             "On host #{target.name}: #{environment} #{full_command}"
           )
           channel.on_data {|p,data| result.output << data }
           channel.on_extended_data {|_,_,data| result.error << data }
           channel.on_request("exit-status") {|p,data| result.exit_code = data.read_long}
         end
       end
       session.loop unless gateway
       result[:success?] = result.exit_code.zero?
       if !result.success? || (result.error.length.nonzero? && !result.exit_code.zero?)
         log.error("#{result.error}\n#{"Output: #{result.output}" unless result.output.empty?}")
         raise RemoteCommandFailed.new(full_command, result) unless capture_error
       end
       result
     ensure
       log.level = verbose ? ::Logger::DEBUG : ::Logger::INFO
     end

     def connect!
       return true if connected?

       handle_errors do
         @session =
           if gateway
             create_gateway_session
           else
             create_regular_session
           end
       end
       true
     end

     def connected?
       if gateway
         session && session.active? ? true :false
       else
         session && !session.closed? ? true : false
       end
     end

     def test_ssh!
       handle_errors do
         test_plain_ssh
       end
     end

     private

     def open_session_channel &block
       if gateway
         options = EXTENDED_OPTIONS.merge(logger: log)
         session.ssh(target.host, target.user, options) do |session|
           session.open_channel(&block)
         end
       else
         session.open_channel(&block)
       end
     end

     def create_regular_session
       Net::SSH.start(
         target.host, target.user, EXTENDED_OPTIONS.merge(logger: log)
       )
     end

     def create_gateway_session
       Net::SSH::Gateway.new(
         gateway.host,
         gateway.user,
         EXTENDED_OPTIONS.merge(logger: log)
       )
     end

     def handle_errors
       attempts = 0
       yield
     rescue Timeout::Error, Errno::ETIMEDOUT, Errno::ECONNREFUSED => e
       raise SshConnectionError.new(
         host: target.host,
         message: e.message
       )
     rescue Net::SSH::HostKeyMismatch => e
       attempts += 1
       log.error("Mismatch of host keys, #{attempts}. attempt, fixing and going to retry now..")
       e.remember_host!
       retry unless attempts > 1
       log.error("Mismatch of host keys, #{attempts}. attempt, failing..")
       raise
     end

     def test_plain_ssh
       opts = EXTENDED_OPTIONS.merge(
         logger: log
       )
       Net::SSH::Transport::Session.new(target.host, opts)
     end

     def set_environment params
       return "" if params.empty?

       source_files = params.delete(:source) || []
       export_env = params.map {|env| "export #{env[0]}=#{env[1]}; " }.join.strip
       source_env = source_files.map {|file| "source #{file}; "}.join
       env = source_env + export_env
       log.warn("Updating environment with `#{env}`") unless env.strip.empty?
       env
     end

  end

  class RemoteCommandFailed < StandardError
    def initialize command, result
      super(
        "`#{command}` failed.\nError: #{result.error}"                +
        "#{ "Output: #{result.output}" unless result.output.empty? }" +
        "Host: #{result.host}"
      )
    end
  end

  class SshConnectionError < StandardError
    def initialize options={}
      message = "SSH connection to #{options[:host]} failed\n#{options[:message]}"
      message << "\nTimeout #{options[:timeout]} seconds" if options[:timeout]
      super(message)
    end
  end
end
