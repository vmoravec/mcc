desc "Show all available AWS VPCs under currently active profile"
command :vpc do |vpc|
  vpc.flag :name, :n, desc: "Vpc name", required: false
  vpc.action do |global_options, options, args|
    if options[:name]
      get_vpc(options[:name])
      show_vpc_details(options[:name])
    else
      get_all_vpcs
      show_vpcs_details
    end
  end

  vpc.desc "List changesets for a specific vpc"
  vpc.command :changesets, :cs do |cs|
    cs.flag :"stack-name", :n, desc: "Vpc name"
    cs.action do |global, options, args|
      get_vpc_changesets(options)
      show_vpc_changesets(options)
    end
  end
end

def get_vpc(name)
  command = "aws ec2 describe-vpcs --vpc-id #{name}"
  result = exec!(:local, shell: command)
  data[:aws][:vpc] = JSON.parse(result.output)
end

def show_vpc_details(name)
end

def get_all_vpcs
  command = "aws ec2 describe-vpcs"
  result = exec!(:local, shell: command)
  data[:aws][:vpc] = JSON.parse(result.output)
end

def show_vpcs_details
  json = data[:aws][:vpc]
  vpcs = json["Vpcs"].map do |vpc|
    tags = vpc['Tags'].flatten
    name_tag = tags.find {|h| h["Key"] == "Name" }["Value"]
    stack_name_tag = tags.find {|h| h["Key"] == "aws:cloudformation:stack-name" }
    [
      stack_name_tag && stack_name_tag["Value"],
      vpc['CidrBlock'],
      vpc['State'],
      vpc['VpcId'],
      name_tag
    ]
  end
  headings = ["Stack Name", 'Cidr', 'State', 'Vpc ID', "Tag Name"]
  log.info Terminal::Table.new(headings: headings, rows: vpcs)
end

def get_vpc_changesets(options)
  command = "aws cloudformation list-change-sets --stack-name #{options[:n]}"
  result = exec!(:local, shell: command)
  data[:aws][:vpc] = Hash.new({})
  data[:aws][:vpc][:changesets] = JSON.parse(result.output)
end

def show_vpc_changesets(options)
  json = data[:aws][:vpc][:changesets]
  return log.info "No changesets found for stack #{options[:n]} " if json["Summaries"].empty?
  log.info json["Summaries"]
end
