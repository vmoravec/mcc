desc "Show current system environment"
command :env do |env|
  env.desc "Show system config for cloud CLIs"
  env.action do |global_options, _, _|
    get_active_profile(:aws)
    get_profile(:aws)
    show_env_details(:aws, global_options)
    show_hints(:aws, global_options)

    #result =  exec!(:remote, host: 'localhost', shell: "aws", args: 'ec2 desribe-instances')
    #puts result.output
  end
end

def get_active_profile(provider_name)
  case provider_name
  when :aws
    data[:aws] = { current_profile: ENV['AWS_PROFILE'] }
  end
end

def get_profile(provider_name)
  case provider_name
  when :aws
    get_aws_config
  end
end

def get_aws_config
  data[:aws][:config] = {}
  data[:aws][:config][:sections] = IniFile.load("#{ENV['HOME']}/.aws/config").sections.map do |section|
    section.split.last
  end
end

def show_env_details(provider, options)
    case provider
    when :aws
      if options[:format]['json']
        puts data_to_json(:aws)
        return
      end

      log.info "Current profile set for AWS is #{data[:aws][:current_profile]}"
      log.info "Available profiles: #{data[:aws][:config][:sections].join(", ")}"

      return if options[:cloud] == 'aws'
    end
end

def show_hints(provider, global_options)
  return if global_options[:format] == 'json'

  case provider
  when :aws
    log.info "To change your current profile set `export AWS_PROFILE=other-shiny-profile` before running mcc command or insert into your bash profile"
  end
end
