require 'socket'

module Mcc
  class Target < OpenStruct
    Gateway = Struct.new(:name, :ip)

    class Host < Struct.new(:name, :ip)
      def initialize *attributes
        super
        if !attributes.include?(:ip)
          ip = Socket.ip_address_list.find do |ai|
            ai.ipv4? && !ai.ipv4_loopback?
          end
          self.ip = ip.ip_address
        end
      end
    end

    def initialize attrs={}
      super
      gateway_attrs = attrs[:gateway] || {}
      @host = Host.new(attrs[:name], attrs[:ip])
      @gateway = Gateway.new(gateway_attrs[:name], gateway_attrs[:ip])
    end

    def attributes
      self.to_h
    end
  end
end
