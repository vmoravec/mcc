
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "mcc/version"

Gem::Specification.new do |spec|
  spec.name          = "mcc"
  spec.version       = Mcc::VERSION
  spec.authors       = ["Vladimir Moravec"]

  spec.summary       = %q{Multi cloud CLI}
  spec.description   = %q{Single CLI for public clouds}
  spec.homepage      = "https://github.com/vmoravec/mcc"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = ""
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features|.bundle|.gitignore)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"

  spec.add_runtime_dependency "gli",             "~> 2.18"
  spec.add_runtime_dependency "net-ssh",         "~> 4.2"
  spec.add_runtime_dependency "net-ssh-gateway", "~> 2.0"
  spec.add_runtime_dependency "net-scp",         "~> 1.2"
  spec.add_runtime_dependency "terminal-table",  "~> 1.8"
  spec.add_runtime_dependency 'awesome_print',   "~> 1.8"
  spec.add_runtime_dependency 'inifile',         "~> 3.0"
end
